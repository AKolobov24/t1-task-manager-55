package ru.t1.akolobov.tm.logger.component;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.akolobov.tm.logger.api.IConsumerService;
import ru.t1.akolobov.tm.logger.api.IPropertyService;

@Component
public class Bootstrap {

    @NotNull
    @Autowired
    private IConsumerService consumerService;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    public void init() throws Exception {
        consumerService.subscribe(propertyService.getConsumerQueue());
    }

}

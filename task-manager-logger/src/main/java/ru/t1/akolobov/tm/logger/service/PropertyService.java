package ru.t1.akolobov.tm.logger.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.t1.akolobov.tm.logger.api.IPropertyService;

import java.util.Properties;

@Service
public final class PropertyService implements IPropertyService {

    @NotNull
    private static final String FILE_NAME = "application.properties";

    @NotNull
    private static final String EMPTY_VALUE = "---";

    @NotNull
    private static final String SERVER_ENABLED = "server.enabled";

    @NotNull
    private static final String CONSUMER_URL = "consumer.url";

    @NotNull
    private static final String CONSUMER_QUEUE = "consumer.queue";

    @NotNull
    private static final String DATABASE_HOST = "database.host";

    @NotNull
    private static final String DATABASE_PORT = "database.port";

    @NotNull
    private static final String DATABASE_NAME = "database.name";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        properties.load(ClassLoader.getSystemResourceAsStream(FILE_NAME));
    }

    @NotNull
    @Override
    public String getServerEnabled() {
        return getStringValue(SERVER_ENABLED);
    }

    @NotNull
    @Override
    public String getConsumerUrl() {
        return getStringValue(CONSUMER_URL);
    }

    @NotNull
    @Override
    public String getConsumerQueue() {
        return getStringValue(CONSUMER_QUEUE);
    }

    @NotNull
    @Override
    public String getDatabaseHost() {
        return getStringValue(DATABASE_HOST);
    }

    @NotNull
    @Override
    public String getDatabasePort() {
        return getStringValue(DATABASE_PORT);
    }

    @NotNull
    @Override
    public String getDatabaseName() {
        return getStringValue(DATABASE_NAME);
    }

    @NotNull
    @Override
    public String getEmptyValue() {
        return EMPTY_VALUE;
    }

    @NotNull
    private String getStringValue(@NotNull final String key) {
        return getStringValue(key, EMPTY_VALUE);
    }

    @NotNull
    private String getStringValue(@NotNull final String key, @NotNull final String defaultValue) {
        // в параметрах запуска
        if (System.getProperties().containsKey(key)) return System.getProperties().getProperty(key);
        // в переменных среды
        @NotNull String envKey = getEnvKey(key);
        if (System.getenv().containsKey(envKey)) return System.getenv(envKey);
        // в файле настройки
        return properties.getProperty(key, defaultValue);
    }

    @NotNull
    private String getEnvKey(@NotNull final String key) {
        return key.replace(".", "_").toUpperCase();
    }

    @NotNull
    private Integer getIntValue(@NotNull final String key, @NotNull final String defaultValue) {
        return Integer.parseInt(getStringValue(key, defaultValue));
    }

}

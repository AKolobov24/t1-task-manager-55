package ru.t1.akolobov.tm.data.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.model.Session;
import ru.t1.akolobov.tm.model.User;

import java.util.ArrayList;
import java.util.List;

public final class TestSession {

    @NotNull
    public static Session createSession(@NotNull final User user) {
        @NotNull Session session = new Session();
        session.setUser(user);
        return session;
    }

    @NotNull
    public static List<Session> createSessionList(@NotNull final User user) {
        @NotNull List<Session> sessionList = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            @NotNull Session session = new Session();
            session.setUser(user);
            sessionList.add(session);
        }
        return sessionList;
    }

}

package ru.t1.akolobov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.akolobov.tm.api.repository.model.ISessionRepository;
import ru.t1.akolobov.tm.api.repository.model.IUserRepository;
import ru.t1.akolobov.tm.api.service.IConnectionService;
import ru.t1.akolobov.tm.api.service.model.ISessionService;
import ru.t1.akolobov.tm.exception.field.IdEmptyException;
import ru.t1.akolobov.tm.exception.field.UserIdEmptyException;
import ru.t1.akolobov.tm.marker.UnitCategory;
import ru.t1.akolobov.tm.model.Session;
import ru.t1.akolobov.tm.repository.model.SessionRepository;
import ru.t1.akolobov.tm.repository.model.UserRepository;
import ru.t1.akolobov.tm.service.ConnectionService;
import ru.t1.akolobov.tm.service.PropertyService;

import javax.persistence.EntityManager;
import java.util.List;

import static ru.t1.akolobov.tm.data.model.TestSession.createSession;
import static ru.t1.akolobov.tm.data.model.TestSession.createSessionList;
import static ru.t1.akolobov.tm.data.model.TestUser.*;

@Category(UnitCategory.class)
public class SessionServiceTest {

    @NotNull
    private final static PropertyService propertyService = new PropertyService();
    @NotNull
    private final static IConnectionService connectionService = new ConnectionService(propertyService);
    @NotNull
    private final static EntityManager repositoryEntityManager = connectionService.getEntityManager();
    @NotNull
    private final static IUserRepository userRepository = new UserRepository(repositoryEntityManager);
    @NotNull
    private final ISessionRepository repository = new SessionRepository(repositoryEntityManager);
    @NotNull
    private final ISessionService service = new SessionService(connectionService);

    @BeforeClass
    public static void addUsers() {
        repositoryEntityManager.getTransaction().begin();
        userRepository.add(USER1);
        userRepository.add(USER2);
        repositoryEntityManager.getTransaction().commit();
    }

    @AfterClass
    public static void clearUsers() {
        repositoryEntityManager.getTransaction().begin();
        userRepository.remove(USER1);
        userRepository.remove(USER2);
        repositoryEntityManager.getTransaction().commit();
        repositoryEntityManager.close();
    }

    @Before
    public void initRepository() {
        repositoryEntityManager.getTransaction().begin();
        createSessionList(USER1).forEach(repository::add);
        repositoryEntityManager.getTransaction().commit();
    }

    @After
    public void clearRepository() {
        repositoryEntityManager.getTransaction().begin();
        repository.clear(USER1_ID);
        repository.clear(USER2_ID);
        repositoryEntityManager.getTransaction().commit();
    }

    @Test
    public void add() {
        @NotNull final Session session = createSession(USER1);
        service.add(USER1_ID, session);
        Assert.assertEquals(session, repository.findOneById(session.getUser().getId(), session.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.add(USER_EMPTY_ID, session));
    }

    @Test
    public void clear() {
        Assert.assertThrows(UserIdEmptyException.class, () -> service.clear(USER_EMPTY_ID));
        @NotNull final List<Session> sessionList = createSessionList(USER2);
        service.add(sessionList);
        Assert.assertFalse(service.findAll(USER2_ID).isEmpty());
        service.clear(USER2_ID);
        Assert.assertTrue(service.findAll(USER2_ID).isEmpty());
    }

    @Test
    public void existById() {
        @NotNull final Session session = createSession(USER1);
        service.add(session);
        Assert.assertTrue(service.existById(USER1_ID, session.getId()));
        Assert.assertFalse(service.existById(USER2_ID, session.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> service.existById(USER1_ID, USER_EMPTY_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.existById(USER_EMPTY_ID, session.getId()));
    }

    @Test
    public void findAll() {
        @NotNull final List<Session> sessionList = createSessionList(USER2);
        service.add(sessionList);
        Assert.assertEquals(sessionList, service.findAll(USER2_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findAll(USER_EMPTY_ID));
    }

    @Test
    public void findOneById() {
        @NotNull final Session session = createSession(USER1);
        service.add(session);
        Assert.assertEquals(session, service.findOneById(USER1_ID, session.getId()));
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.findOneById(USER_EMPTY_ID, session.getId())
        );
        Assert.assertThrows(
                IdEmptyException.class,
                () -> service.findOneById(USER1_ID, USER_EMPTY_ID)
        );
    }

    @Test
    public void getSize() {
        int size = service.findAll(USER1_ID).size();
        Assert.assertEquals(size, service.getSize(USER1_ID).intValue());
        service.add(createSession(USER1));
        Assert.assertEquals(size + 1, service.getSize(USER1_ID).intValue());
        Assert.assertThrows(UserIdEmptyException.class, () -> service.getSize(USER_EMPTY_ID));
    }

    @Test
    public void remove() {
        @NotNull final List<Session> sessionList = service.findAll(USER1_ID);
        int size = sessionList.size();
        @NotNull final Session session = sessionList.get(size - 1);
        Assert.assertNotNull(session);
        service.remove(USER1_ID, session);
        Assert.assertFalse(service.findAll(USER1_ID).contains(session));
        Assert.assertNull(service.findOneById(USER1_ID, session.getId()));
        Assert.assertEquals(size - 1, service.findAll(USER1_ID).size());
        Assert.assertThrows(UserIdEmptyException.class, () -> service.remove(USER_EMPTY_ID, session));
    }

    @Test
    public void removeById() {
        @NotNull final List<Session> sessionList = service.findAll(USER1_ID);
        int size = sessionList.size();
        @NotNull final Session session = sessionList.get(size - 1);
        Assert.assertNotNull(session);
        service.removeById(USER1_ID, session.getId());
        Assert.assertFalse(service.findAll(USER1_ID).contains(session));
        Assert.assertNull(service.findOneById(USER1_ID, session.getId()));
        Assert.assertEquals(size - 1, service.findAll(USER1_ID).size());
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeById(USER_EMPTY_ID, session.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> service.removeById(USER1_ID, USER_EMPTY_ID));
    }

}

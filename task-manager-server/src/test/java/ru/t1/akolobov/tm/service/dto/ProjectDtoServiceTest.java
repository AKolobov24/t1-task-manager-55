package ru.t1.akolobov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.akolobov.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.akolobov.tm.api.repository.dto.IUserDtoRepository;
import ru.t1.akolobov.tm.api.service.IConnectionService;
import ru.t1.akolobov.tm.api.service.dto.IProjectDtoService;
import ru.t1.akolobov.tm.dto.model.ProjectDto;
import ru.t1.akolobov.tm.enumerated.Sort;
import ru.t1.akolobov.tm.enumerated.Status;
import ru.t1.akolobov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.akolobov.tm.exception.field.IdEmptyException;
import ru.t1.akolobov.tm.exception.field.NameEmptyException;
import ru.t1.akolobov.tm.exception.field.StatusEmptyException;
import ru.t1.akolobov.tm.exception.field.UserIdEmptyException;
import ru.t1.akolobov.tm.marker.UnitCategory;
import ru.t1.akolobov.tm.repository.dto.ProjectDtoRepository;
import ru.t1.akolobov.tm.repository.dto.UserDtoRepository;
import ru.t1.akolobov.tm.service.ConnectionService;
import ru.t1.akolobov.tm.service.PropertyService;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

import static ru.t1.akolobov.tm.data.dto.TestProjectDto.createProject;
import static ru.t1.akolobov.tm.data.dto.TestProjectDto.createProjectList;
import static ru.t1.akolobov.tm.data.dto.TestUserDto.*;

@Category(UnitCategory.class)
public class ProjectDtoServiceTest {

    @NotNull
    private final static PropertyService propertyService = new PropertyService();

    @NotNull
    private final static IConnectionService connectionService =
            new ConnectionService(propertyService);

    @NotNull
    private final static EntityManager repositoryEntityManager =
            connectionService.getEntityManager();

    @NotNull
    private final static IProjectDtoRepository repository =
            new ProjectDtoRepository(repositoryEntityManager);

    @NotNull
    private final static IUserDtoRepository userRepository =
            new UserDtoRepository(repositoryEntityManager);

    @NotNull
    private final IProjectDtoService service = new ProjectDtoService(connectionService);

    @BeforeClass
    public static void addUsers() {
        repositoryEntityManager.getTransaction().begin();
        userRepository.add(USER1);
        userRepository.add(USER2);
        repositoryEntityManager.getTransaction().commit();
    }

    @AfterClass
    public static void clearUsers() {
        repositoryEntityManager.getTransaction().begin();
        userRepository.remove(USER1);
        userRepository.remove(USER2);
        repositoryEntityManager.getTransaction().commit();
        repositoryEntityManager.close();
    }

    @Before
    public void initRepository() {
        repositoryEntityManager.getTransaction().begin();
        createProjectList(USER1_ID).forEach(repository::add);
        repositoryEntityManager.getTransaction().commit();
    }

    @After
    public void clearRepository() {
        repositoryEntityManager.getTransaction().begin();
        repository.clear(USER1_ID);
        repository.clear(USER2_ID);
        repositoryEntityManager.getTransaction().commit();
    }

    @Test
    public void add() {
        @NotNull final ProjectDto project = createProject(USER1_ID);
        service.add(USER1_ID, project);
        Assert.assertEquals(project, repository.findOneById(project.getUserId(), project.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.add(USER_EMPTY_ID, project));
    }

    @Test
    public void clear() {
        Assert.assertThrows(UserIdEmptyException.class, () -> service.clear(USER_EMPTY_ID));
        @NotNull final List<ProjectDto> projectList = createProjectList(USER2_ID);
        service.add(projectList);
        Assert.assertFalse(service.findAll(USER2_ID).isEmpty());
        service.clear(USER2_ID);
        Assert.assertTrue(service.findAll(USER2_ID).isEmpty());
    }

    @Test
    public void existById() {
        @NotNull final ProjectDto project = createProject(USER1_ID);
        service.add(USER1_ID, project);
        Assert.assertTrue(service.existById(USER1_ID, project.getId()));
        Assert.assertFalse(service.existById(USER2_ID, project.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> service.existById(USER1_ID, USER_EMPTY_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.existById(USER_EMPTY_ID, project.getId()));
    }

    @Test
    public void findAll() {
        @NotNull final List<ProjectDto> projectList = createProjectList(USER2_ID);
        service.add(projectList);
        Assert.assertEquals(projectList, service.findAll(USER2_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findAll(USER_EMPTY_ID));
    }


    @Test
    public void findAllSorted() {
        @NotNull final ProjectDto project = createProject(USER1_ID);
        project.setName("project-0");
        project.setDescription("project-0-desc");
        @NotNull final List<ProjectDto> projectList = new ArrayList<>();
        projectList.add(project);
        projectList.addAll(service.findAll(USER1_ID));
        service.add(USER1_ID, project);
        @NotNull final List<ProjectDto> projectList2 = service.findAll(USER1_ID, Sort.BY_NAME);
        Assert.assertEquals(projectList, projectList2);
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.findAll(USER_EMPTY_ID, Sort.BY_NAME)
        );
    }

    @Test
    public void findOneById() {
        @NotNull final ProjectDto project = createProject(USER1_ID);
        service.add(USER1_ID, project);
        Assert.assertEquals(project, service.findOneById(USER1_ID, project.getId()));
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.findOneById(USER_EMPTY_ID, project.getId())
        );
        Assert.assertThrows(
                IdEmptyException.class,
                () -> service.findOneById(USER1_ID, USER_EMPTY_ID)
        );
    }

    @Test
    public void getSize() {
        int size = service.findAll(USER1_ID).size();
        Assert.assertEquals(size, service.getSize(USER1_ID).intValue());
        service.add(USER1_ID, createProject(USER1_ID));
        Assert.assertEquals(size + 1, service.getSize(USER1_ID).intValue());
        Assert.assertThrows(UserIdEmptyException.class, () -> service.getSize(USER_EMPTY_ID));
    }

    @Test
    public void remove() {
        @NotNull final List<ProjectDto> projectList = service.findAll(USER1_ID);
        int size = projectList.size();
        @NotNull final ProjectDto project = projectList.get(size - 1);
        Assert.assertNotNull(project);
        service.remove(USER1_ID, project);
        Assert.assertFalse(service.findAll(USER1_ID).contains(project));
        Assert.assertNull(service.findOneById(USER1_ID, project.getId()));
        Assert.assertEquals(size - 1, service.findAll(USER1_ID).size());
        Assert.assertThrows(UserIdEmptyException.class, () -> service.remove(USER_EMPTY_ID, project));
    }

    @Test
    public void removeById() {
        @NotNull final List<ProjectDto> projectList = service.findAll(USER1_ID);
        int size = projectList.size();
        @NotNull final ProjectDto project = projectList.get(size - 1);
        Assert.assertNotNull(project);
        service.removeById(USER1_ID, project.getId());
        Assert.assertFalse(service.findAll(USER1_ID).contains(project));
        Assert.assertNull(service.findOneById(USER1_ID, project.getId()));
        Assert.assertEquals(size - 1, service.findAll(USER1_ID).size());
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeById(USER_EMPTY_ID, project.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> service.removeById(USER1_ID, USER_EMPTY_ID));
    }

    @Test
    public void changeStatusById() {
        ProjectDto project = createProject(USER1_ID);
        service.add(USER1_ID, project);
        String newProjectId = project.getId();
        Assert.assertNotNull(service.changeStatusById(USER1_ID, newProjectId, Status.IN_PROGRESS));
        project = service.findOneById(USER1_ID, newProjectId);
        Assert.assertNotNull(project);
        Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.changeStatusById(USER_EMPTY_ID, newProjectId, Status.IN_PROGRESS)
        );
        Assert.assertThrows(
                IdEmptyException.class,
                () -> service.changeStatusById(USER1_ID, USER_EMPTY_ID, Status.IN_PROGRESS)
        );
        Assert.assertThrows(
                StatusEmptyException.class,
                () -> service.changeStatusById(USER1_ID, newProjectId, null)
        );
        Assert.assertThrows(
                ProjectNotFoundException.class,
                () -> service.changeStatusById(USER2_ID, newProjectId, Status.IN_PROGRESS)
        );
    }

    @Test
    public void create() {
        @NotNull final ProjectDto project = createProject(USER1_ID);
        ProjectDto newProject = service.create(USER1_ID, project.getName());
        Assert.assertNotNull(newProject);
        newProject = service.findOneById(USER1_ID, newProject.getId());
        Assert.assertNotNull(newProject);
        Assert.assertEquals(project.getName(), newProject.getName());

        newProject = service.create(USER1_ID, project.getName(), project.getDescription());
        Assert.assertNotNull(newProject);
        newProject = service.findOneById(USER1_ID, newProject.getId());
        Assert.assertNotNull(newProject);
        Assert.assertEquals(project.getName(), newProject.getName());
        Assert.assertEquals(project.getDescription(), newProject.getDescription());

        project.setStatus(Status.IN_PROGRESS);
        newProject = service.create(USER1_ID, project.getName(), project.getStatus());
        Assert.assertNotNull(newProject);
        newProject = service.findOneById(USER1_ID, newProject.getId());
        Assert.assertNotNull(newProject);
        Assert.assertEquals(project.getName(), newProject.getName());
        Assert.assertEquals(project.getStatus(), newProject.getStatus());

        Assert.assertThrows(UserIdEmptyException.class, () -> service.create(USER_EMPTY_ID, project.getName()));
        Assert.assertThrows(NameEmptyException.class, () -> service.create(USER1_ID, ""));
    }

    @Test
    public void updateById() {
        @NotNull final ProjectDto project = service.findAll(USER1_ID).get(0);
        Assert.assertNotNull(project);
        @NotNull final String newName = "NewName";
        @NotNull final String newDescription = "NewDescription";
        service.updateById(USER1_ID, project.getId(), newName, newDescription);
        final ProjectDto newProject = service.findOneById(USER1_ID, project.getId());
        Assert.assertNotNull(newProject);
        Assert.assertEquals(newName, newProject.getName());
        Assert.assertEquals(newDescription, newProject.getDescription());

        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.updateById(USER_EMPTY_ID, project.getId(), newName, newDescription)
        );
        Assert.assertThrows(
                IdEmptyException.class,
                () -> service.updateById(USER1_ID, USER_EMPTY_ID, newName, newDescription)
        );
        Assert.assertThrows(
                NameEmptyException.class,
                () -> service.updateById(USER1_ID, project.getId(), "", newDescription)
        );
        Assert.assertThrows(
                ProjectNotFoundException.class,
                () -> service.updateById(USER2_ID, project.getId(), newName, newDescription)
        );
    }

}

package ru.t1.akolobov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.api.repository.dto.IDtoRepository;
import ru.t1.akolobov.tm.dto.model.AbstractDtoModel;

import javax.persistence.EntityManager;
import java.util.Collection;

public abstract class AbstractDtoRepository<M extends AbstractDtoModel> implements IDtoRepository<M> {

    @NotNull
    protected final EntityManager entityManager;

    public AbstractDtoRepository(@NotNull EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void add(@NotNull final M model) {
        entityManager.persist(model);
    }

    @Override
    public void update(@NotNull final M model) {
        entityManager.merge(model);
    }

    @NotNull
    @Override
    public Collection<M> add(@NotNull Collection<M> models) {
        models.forEach(this::add);
        return models;
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull Collection<M> models) {
        clear();
        return add(models);
    }

    @Override
    public boolean existById(@NotNull final String id) {
        return findOneById(id) != null;
    }

    @Override
    public void remove(@NotNull final M model) {
        entityManager.remove(model);
    }

}

package ru.t1.akolobov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.api.endpoint.IUserEndpoint;
import ru.t1.akolobov.tm.api.service.IServiceLocator;
import ru.t1.akolobov.tm.api.service.dto.IUserDtoService;
import ru.t1.akolobov.tm.dto.model.UserDto;
import ru.t1.akolobov.tm.dto.request.*;
import ru.t1.akolobov.tm.dto.response.*;
import ru.t1.akolobov.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.t1.akolobov.tm.api.endpoint.IUserEndpoint")
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    private final IUserDtoService userService;

    public UserEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
        this.userService = getServiceLocator().getUserService();
    }

    @NotNull
    @Override
    @WebMethod
    public UserChangePasswordResponse changePassword(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserChangePasswordRequest request
    ) {
        @NotNull final String userId = check(request).getUserId();
        userService.setPassword(userId, request.getPassword());
        return new UserChangePasswordResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserLockResponse lockUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserLockRequest request
    ) {
        check(request, Role.ADMIN);
        userService.lockUserByLogin(request.getLogin());
        return new UserLockResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserRegistryResponse registryUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserRegistryRequest request
    ) {
        @NotNull UserDto user = userService.create(
                request.getLogin(),
                request.getPassword(),
                request.getEmail()
        );
        return new UserRegistryResponse(user);
    }

    @NotNull
    @Override
    @WebMethod
    public UserRemoveResponse removeUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserRemoveRequest request
    ) {
        check(request, Role.ADMIN);
        userService.removeByLogin(request.getLogin());
        return new UserRemoveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserUnlockResponse unlockUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserUnlockRequest request
    ) {
        check(request, Role.ADMIN);
        userService.unlockUserByLogin(request.getLogin());
        return new UserUnlockResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserUpdateProfileResponse updateProfile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserUpdateProfileRequest request
    ) {
        @NotNull final String userId = check(request).getUserId();
        return new UserUpdateProfileResponse(
                userService.updateUser(
                        userId,
                        request.getFirstName(),
                        request.getLastName(),
                        request.getMiddleName()
                )
        );
    }

}

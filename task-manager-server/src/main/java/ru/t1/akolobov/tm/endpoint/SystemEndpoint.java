package ru.t1.akolobov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.api.endpoint.ISystemEndpoint;
import ru.t1.akolobov.tm.api.service.IPropertyService;
import ru.t1.akolobov.tm.api.service.IServiceLocator;
import ru.t1.akolobov.tm.dto.request.ApplicationAboutRequest;
import ru.t1.akolobov.tm.dto.request.ApplicationSystemInfoRequest;
import ru.t1.akolobov.tm.dto.request.ApplicationVersionRequest;
import ru.t1.akolobov.tm.dto.response.ApplicationAboutResponse;
import ru.t1.akolobov.tm.dto.response.ApplicationSystemInfoResponse;
import ru.t1.akolobov.tm.dto.response.ApplicationVersionResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.t1.akolobov.tm.api.endpoint.ISystemEndpoint")
public final class SystemEndpoint implements ISystemEndpoint {

    @NotNull
    private final IServiceLocator serviceLocator;

    public SystemEndpoint(@NotNull IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    @Override
    @WebMethod
    public ApplicationAboutResponse getAbout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ApplicationAboutRequest request
    ) {
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @NotNull final ApplicationAboutResponse response = new ApplicationAboutResponse();
        response.setName(propertyService.getApplicationAuthor());
        response.setEmail(propertyService.getAuthorEmail());
        return response;
    }

    @NotNull
    @Override
    @WebMethod
    public ApplicationVersionResponse getVersion(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ApplicationVersionRequest request
    ) {
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @NotNull final ApplicationVersionResponse response = new ApplicationVersionResponse();
        response.setVersion(propertyService.getApplicationVersion());
        return response;
    }

    @NotNull
    @Override
    public ApplicationSystemInfoResponse getSystemInfo(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ApplicationSystemInfoRequest request
    ) {
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @NotNull final ApplicationSystemInfoResponse response = new ApplicationSystemInfoResponse();
        response.setHostName(propertyService.getServerHost());
        response.setProcessors(Runtime.getRuntime().availableProcessors());
        response.setFreeMemory(Runtime.getRuntime().freeMemory());
        response.setMaxMemory(Runtime.getRuntime().maxMemory());
        response.setTotalMemory(Runtime.getRuntime().totalMemory());
        return response;
    }

}

package ru.t1.akolobov.tm.dto;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1.akolobov.tm.enumerated.OperationType;

@Getter
@Setter
@NoArgsConstructor
public class OperationEvent {

    private OperationType type;

    private Object entity;

    private String table;

    private long timestamp = System.currentTimeMillis();

    public OperationEvent(OperationType type, Object entity) {
        this.type = type;
        this.entity = entity;
    }

}

package ru.t1.akolobov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.api.repository.model.ITaskRepository;
import ru.t1.akolobov.tm.enumerated.Sort;
import ru.t1.akolobov.tm.model.Task;

import javax.persistence.EntityManager;
import java.util.List;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    public TaskRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void clear() {
        @NotNull String jpql = "DELETE FROM Task";
        entityManager.createQuery(jpql)
                .executeUpdate();
    }

    @Nullable
    @Override
    public Task findOneById(@NotNull String id) {
        return entityManager.find(Task.class, id);
    }

    @NotNull
    @Override
    public List<Task> findAll() {
        @NotNull String jpql = "SELECT t FROM Task t";
        return entityManager.createQuery(jpql, Task.class)
                .getResultList();
    }

    @Override
    public @NotNull List<Task> findAll(@NotNull Sort sort) {
        @NotNull String jpql = "SELECT t FROM Task t ORDER BY t." + sort.getColumnName();
        return entityManager.createQuery(jpql, Task.class)
                .getResultList();
    }

    @Override
    public Long getSize() {
        @NotNull String jpql = "SELECT COUNT(t) FROM Task t";
        return entityManager.createQuery(jpql, Long.class)
                .getSingleResult();
    }

    @Override
    public void removeById(@NotNull String id) {
        Task task = entityManager.find(Task.class, id);
        if (task == null) return;
        entityManager.remove(task);
    }

    @Override
    public void clear(@NotNull String userId) {
        @NotNull String jpql = "DELETE FROM Task t WHERE t.user.id = :userId";
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @NotNull
    @Override
    public List<Task> findAll(@NotNull String userId) {
        @NotNull String jpql = "SELECT t FROM Task t WHERE t.user.id = :userId";
        return entityManager.createQuery(jpql, Task.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Task> findAll(@NotNull String userId, @NotNull Sort sort) {
        @NotNull String jpql = "SELECT t FROM Task t WHERE t.user.id = :userId " +
                "ORDER BY t." + sort.getColumnName();
        return entityManager.createQuery(jpql, Task.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public Task findOneById(@NotNull String userId, @NotNull String id) {
        @NotNull String jpql = "SELECT t FROM Task t " +
                "WHERE t.id = :id AND t.user.id = :userId";
        return entityManager.createQuery(jpql, Task.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList().stream().findFirst()
                .orElse(null);
    }

    @Override
    public Long getSize(@NotNull String userId) {
        @NotNull String jpql = "SELECT COUNT(t) FROM Task t WHERE t.user.id = :userId";
        return entityManager.createQuery(jpql, Long.class)
                .setParameter("userId", userId)
                .getSingleResult();
    }

    @Override
    public void removeById(@NotNull String userId, @NotNull String id) {
        Task task = findOneById(userId, id);
        if (task == null) return;
        entityManager.remove(task);
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull String jpql = "SELECT t FROM Task t " +
                "WHERE t.user.id = :userId AND t.project.id = :projectId";
        return entityManager.createQuery(jpql, Task.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

}

package ru.t1.akolobov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.enumerated.FileFormat;

public interface IDomainService {

    void saveDataBackup();

    void saveDataBase64();

    void saveDataBinary();

    void saveData(FileFormat format);

    void saveDataJaxb(FileFormat format);

    void loadDataBackup();

    void loadDataBase64();

    void loadDataBinary();

    void loadData(FileFormat format);

    void loadDataJaxb(FileFormat format);

    @NotNull
    String getBackupFilePath();

}

package ru.t1.akolobov.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.api.service.IExternalLogService;
import ru.t1.akolobov.tm.dto.OperationEvent;

import javax.jms.*;
import javax.persistence.Table;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public final class ExternalLogService implements IExternalLogService {

    private Connection connection;

    private Session session;

    private MessageProducer messageProducer;

    private boolean isDefined = true;

    private final ExecutorService executorService = Executors.newCachedThreadPool();

    private final ObjectMapper objectMapper = new ObjectMapper();

    private final ObjectWriter objectWriter = objectMapper.writerWithDefaultPrettyPrinter();

    @SneakyThrows
    public ExternalLogService(PropertyService propertyService) {
        @NotNull final String url = propertyService.getExternalLoggerUrl();
        @NotNull final String queue = propertyService.getExternalLoggerQueue();
        if (url.equals(propertyService.getEmptyValue())) {
            isDefined = false;
            System.out.println("WARNING: External logger was not initialized!");
            return;
        }
        ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(url);
        try {
            connection = connectionFactory.createConnection();
        } catch (Exception e) {
            System.err.println(
                    "ERROR: ExternalLogService was not initialized due to: \n" +
                            "\t" + e.getMessage()
            );
            e.printStackTrace();
            return;
        }
        connection.start();
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        Queue destination = session.createQueue(queue);
        messageProducer = session.createProducer(destination);
        System.out.println("External logger was initialized!");
    }

    @SneakyThrows
    public void send(@NotNull final String text) {
        final TextMessage message = session.createTextMessage(text);
        messageProducer.send(message);
    }

    @Override
    public void send(@NotNull final OperationEvent event) {
        executorService.submit(() -> sync(event));
    }

    @SneakyThrows
    public void sync(@NotNull final OperationEvent event) {
        @NotNull final Class<?> entityClass = event.getEntity().getClass();
        if (entityClass.isAnnotationPresent(Table.class)) {
            @NotNull final Table table = entityClass.getAnnotation(Table.class);
            event.setTable(table.name());
        }
        send(objectWriter.writeValueAsString(event));
    }

    @Override
    @SneakyThrows
    public void stop() {
        executorService.shutdown();
        if (!isDefined) return;
        session.close();
        connection.close();
    }

    @Override
    public boolean isDefined() {
        return isDefined;
    }

}

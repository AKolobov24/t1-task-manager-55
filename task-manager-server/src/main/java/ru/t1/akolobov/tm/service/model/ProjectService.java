package ru.t1.akolobov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.api.repository.model.IProjectRepository;
import ru.t1.akolobov.tm.api.service.IConnectionService;
import ru.t1.akolobov.tm.api.service.model.IProjectService;
import ru.t1.akolobov.tm.enumerated.Status;
import ru.t1.akolobov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.akolobov.tm.exception.field.IdEmptyException;
import ru.t1.akolobov.tm.exception.field.NameEmptyException;
import ru.t1.akolobov.tm.exception.field.StatusEmptyException;
import ru.t1.akolobov.tm.exception.field.UserIdEmptyException;
import ru.t1.akolobov.tm.model.Project;
import ru.t1.akolobov.tm.repository.model.ProjectRepository;

import javax.persistence.EntityManager;
import java.util.Optional;

public final class ProjectService extends AbstractUserOwnedService<Project> implements IProjectService {

    public ProjectService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    protected IProjectRepository getRepository(@NotNull EntityManager entityManager) {
        return new ProjectRepository(entityManager);
    }

    @NotNull
    @Override
    public Project changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRepository repository = getRepository(entityManager);
            @NotNull final Project project = Optional
                    .ofNullable(repository.findOneById(userId, id))
                    .orElseThrow(ProjectNotFoundException::new);
            project.setStatus(status);
            entityManager.getTransaction().begin();
            repository.update(project);
            entityManager.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Project create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final Project project = new Project(name);
        add(userId, project);
        return project;
    }

    @NotNull
    @Override
    public Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) return create(userId, name);
        @NotNull final Project project = new Project(name, description);
        add(userId, project);
        return project;
    }

    @NotNull
    @Override
    public Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (status == null) return create(userId, name);
        @NotNull final Project project = new Project(name, status);
        add(userId, project);
        return project;
    }

    @NotNull
    @Override
    public Project updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRepository repository = getRepository(entityManager);
            @NotNull final Project project = Optional
                    .ofNullable(repository.findOneById(userId, id))
                    .orElseThrow(ProjectNotFoundException::new);
            project.setName(name);
            project.setDescription(description);
            entityManager.getTransaction().begin();
            repository.update(project);
            entityManager.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
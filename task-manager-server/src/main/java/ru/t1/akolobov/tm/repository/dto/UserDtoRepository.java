package ru.t1.akolobov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.api.repository.dto.IUserDtoRepository;
import ru.t1.akolobov.tm.dto.model.UserDto;
import ru.t1.akolobov.tm.enumerated.Sort;

import javax.persistence.EntityManager;
import java.util.List;

public final class UserDtoRepository extends AbstractDtoRepository<UserDto> implements IUserDtoRepository {

    public UserDtoRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void clear() {
        @NotNull String jpql = "DELETE FROM UserDto";
        entityManager.createQuery(jpql)
                .executeUpdate();
    }

    @Nullable
    @Override
    public UserDto findOneById(@NotNull String id) {
        return entityManager.find(UserDto.class, id);
    }

    @NotNull
    @Override
    public List<UserDto> findAll() {
        @NotNull String jpql = "SELECT u FROM UserDto u";
        return entityManager.createQuery(jpql, UserDto.class)
                .setHint("org.hibernate.cacheable", true)
                .getResultList();
    }

    @Override
    public @NotNull List<UserDto> findAll(@NotNull Sort sort) {
        @NotNull String jpql = "SELECT u FROM UserDto u ORDER BY u." + sort.getColumnName();
        return entityManager.createQuery(jpql, UserDto.class)
                .setHint("org.hibernate.cacheable", true)
                .getResultList();
    }

    @Override
    public Long getSize() {
        @NotNull String jpql = "SELECT COUNT(u) FROM UserDto u";
        return entityManager.createQuery(jpql, Long.class)
                .setHint("org.hibernate.cacheable", true)
                .getSingleResult();
    }

    @Override
    public void removeById(@NotNull String id) {
        UserDto userDTO = entityManager.find(UserDto.class, id);
        if (userDTO == null) return;
        entityManager.remove(userDTO);
    }


    @Nullable
    @Override
    public UserDto findByLogin(@NotNull final String login) {
        @NotNull String jpql = "SELECT u FROM UserDto u WHERE u.login = :login";
        return entityManager.createQuery(jpql, UserDto.class)
                .setHint("org.hibernate.cacheable", true)
                .setParameter("login", login)
                .setMaxResults(1)
                .getResultList().stream().findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public UserDto findByEmail(@NotNull final String email) {
        @NotNull String jpql = "SELECT u FROM UserDto u WHERE u.email = :email";
        return entityManager.createQuery(jpql, UserDto.class)
                .setHint("org.hibernate.cacheable", true)
                .setParameter("email", email)
                .setMaxResults(1)
                .getResultList().stream().findFirst()
                .orElse(null);
    }

    @Override
    public boolean isLoginExist(@NotNull final String login) {
        return findByLogin(login) != null;
    }

    @Override
    public boolean isEmailExist(@NotNull final String email) {
        return findByEmail(email) != null;
    }

}

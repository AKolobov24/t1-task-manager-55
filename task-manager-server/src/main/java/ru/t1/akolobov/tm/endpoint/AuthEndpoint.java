package ru.t1.akolobov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.akolobov.tm.api.service.IServiceLocator;
import ru.t1.akolobov.tm.dto.model.UserDto;
import ru.t1.akolobov.tm.dto.request.UserLoginRequest;
import ru.t1.akolobov.tm.dto.request.UserLogoutRequest;
import ru.t1.akolobov.tm.dto.request.UserProfileRequest;
import ru.t1.akolobov.tm.dto.response.UserLoginResponse;
import ru.t1.akolobov.tm.dto.response.UserLogoutResponse;
import ru.t1.akolobov.tm.dto.response.UserProfileResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.t1.akolobov.tm.api.endpoint.IAuthEndpoint")
public final class AuthEndpoint extends AbstractEndpoint implements IAuthEndpoint {

    public AuthEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    @WebMethod
    public UserLoginResponse login(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserLoginRequest request
    ) {
        try {
            @NotNull final String token = getServiceLocator()
                    .getAuthService()
                    .login(
                            request.getLogin(),
                            request.getPassword()
                    );
            return new UserLoginResponse(token);
        } catch (@NotNull final Exception e) {
            return new UserLoginResponse(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public UserLogoutResponse logout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserLogoutRequest request
    ) {
        getServiceLocator().getAuthService().logout(request.getToken());
        return new UserLogoutResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserProfileResponse profile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserProfileRequest request
    ) {
        @NotNull final String userId = check(request).getUserId();
        @Nullable final UserDto user = getServiceLocator().getUserService().findOneById(userId);
        return new UserProfileResponse(user);
    }

}

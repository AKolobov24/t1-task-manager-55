package ru.t1.akolobov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.api.component.ISaltProvider;

public interface IPropertyService extends ISaltProvider {

    @NotNull
    String getCommandFolder();

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getApplicationAuthor();

    @NotNull
    String getAuthorEmail();

    @NotNull
    Integer getServerPort();

    @NotNull
    String getServerHost();

    @NotNull
    String getSessionKey();

    int getSessionTimeout();

    @NotNull String getExternalLoggerUrl();

    @NotNull String getExternalLoggerQueue();

    @NotNull String getEmptyValue();
}

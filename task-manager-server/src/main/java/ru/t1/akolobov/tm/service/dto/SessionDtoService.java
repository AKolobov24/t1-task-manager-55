package ru.t1.akolobov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.api.repository.dto.ISessionDtoRepository;
import ru.t1.akolobov.tm.api.service.IConnectionService;
import ru.t1.akolobov.tm.api.service.dto.ISessionDtoService;
import ru.t1.akolobov.tm.dto.model.SessionDto;
import ru.t1.akolobov.tm.enumerated.Role;
import ru.t1.akolobov.tm.exception.entity.SessionNotFoundException;
import ru.t1.akolobov.tm.exception.field.DateEmptyException;
import ru.t1.akolobov.tm.exception.field.IdEmptyException;
import ru.t1.akolobov.tm.exception.field.UserIdEmptyException;
import ru.t1.akolobov.tm.exception.user.RoleEmptyException;
import ru.t1.akolobov.tm.repository.dto.SessionDtoRepository;

import javax.persistence.EntityManager;
import java.util.Date;
import java.util.Optional;

public final class SessionDtoService extends AbstractUserOwnedDtoService<SessionDto> implements ISessionDtoService {

    public SessionDtoService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    protected ISessionDtoRepository getRepository(@NotNull EntityManager entityManager) {
        return new SessionDtoRepository(entityManager);
    }

    @NotNull
    @Override
    public SessionDto updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Date date,
            @Nullable final Role role
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (role == null) throw new RoleEmptyException();
        if (date == null) throw new DateEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionDtoRepository repository = getRepository(entityManager);
            @NotNull final SessionDto session = Optional
                    .ofNullable(repository.findOneById(userId, id))
                    .orElseThrow(SessionNotFoundException::new);
            session.setDate(date);
            session.setRole(role);
            entityManager.getTransaction().begin();
            repository.update(session);
            entityManager.getTransaction().commit();
            return session;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}

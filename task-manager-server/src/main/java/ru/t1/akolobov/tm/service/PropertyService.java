package ru.t1.akolobov.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.api.service.IDatabaseProperty;
import ru.t1.akolobov.tm.api.service.IPropertyService;

import java.util.Properties;

public final class PropertyService implements IPropertyService, IDatabaseProperty {

    @NotNull
    private static final String FILE_NAME = "application.properties";

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "buildNumber";

    @NotNull
    private static final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    private static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    private static final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    private static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT = "1324";

    @NotNull
    private static final String PASSWORD_ITERATION_DEFAULT = "4123";

    @NotNull
    private static final String EMPTY_VALUE = "---";

    @NotNull
    private static final String COMMAND_FOLDER_KEY = "command.folder";

    @NotNull
    private static final String COMMAND_FOLDER_DEFAULT = "./";

    @NotNull
    private static final String SERVER_PORT_KEY = "server.port";

    @NotNull
    private static final String SERVER_PORT_DEFAULT = "6060";

    @NotNull
    private static final String SERVER_HOST_KEY = "server.host";

    @NotNull
    private static final String SERVER_HOST_DEFAULT = "0.0.0.0";

    @NotNull
    private static final String SESSION_KEY_KEY = "session.key";

    @NotNull
    private static final String SESSION_TIMEOUT_KEY = "session.timeout";

    @NotNull
    private static final String SESSION_TIMEOUT_DEFAULT = "900";

    @NotNull
    private static final String DATABASE_URL = "database.url";

    @NotNull
    private static final String DATABASE_USER = "database.username";

    @NotNull
    private static final String DATABASE_PASSWORD = "database.password";

    @NotNull
    private static final String DATABASE_DRIVER = "database.driver";

    @NotNull
    private static final String DATABASE_DIALECT = "database.dialect";

    @NotNull
    private static final String DATABASE_DDL_AUTO = "database.ddl-auto";

    @NotNull
    private static final String DATABASE_SHOW_SQL = "database.show-sql";

    @NotNull
    private static final String DATABASE_2LC = "database.second-lvl-cache";
    @NotNull
    private static final String DATABASE_FACTORY_CLASS = "database.factory-class";
    @NotNull
    private static final String DATABASE_USE_QUERY_CACHE = "database.use-query-cache";
    @NotNull
    private static final String DATABASE_USE_MIN_PUTS = "database.use-min-puts";
    @NotNull
    private static final String DATABASE_REGION_PREFIX = "database.region-prefix";
    @NotNull
    private static final String DATABASE_CONFIG_FILE_PATH = "database.config-file-path";

    @NotNull
    private static final String EXTERNAL_LOGGER_URL = "external-logger.url";

    @NotNull
    private static final String EXTERNAL_LOGGER_QUEUE = "external-logger.queue";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        properties.load(ClassLoader.getSystemResourceAsStream(FILE_NAME));
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        return getStringValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @NotNull
    @Override
    public String getCommandFolder() {
        return getStringValue(COMMAND_FOLDER_KEY, COMMAND_FOLDER_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        return getIntValue(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT);
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        return Manifests.read(APPLICATION_VERSION_KEY);
    }

    @NotNull
    @Override
    public String getApplicationAuthor() {
        return Manifests.read(AUTHOR_NAME_KEY);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        return Manifests.read(AUTHOR_EMAIL_KEY);
    }

    @NotNull
    @Override
    public Integer getServerPort() {
        return getIntValue(SERVER_PORT_KEY, SERVER_PORT_DEFAULT);
    }

    @NotNull
    @Override
    public String getServerHost() {
        return getStringValue(SERVER_HOST_KEY, SERVER_HOST_DEFAULT);
    }

    @NotNull
    @Override
    public String getSessionKey() {
        return getStringValue(SESSION_KEY_KEY);
    }

    @Override
    public int getSessionTimeout() {
        return getIntValue(SESSION_TIMEOUT_KEY, SESSION_TIMEOUT_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseUrl() {
        return getStringValue(DATABASE_URL);
    }

    @NotNull
    @Override
    public String getDatabaseUser() {
        return getStringValue(DATABASE_USER);
    }

    @NotNull
    @Override
    public String getDatabasePassword() {
        return getStringValue(DATABASE_PASSWORD);
    }

    @NotNull
    @Override
    public String getDatabaseDriver() {
        return getStringValue(DATABASE_DRIVER);
    }

    @Override
    @NotNull
    public String getDatabaseDialect() {
        return getStringValue(DATABASE_DIALECT);
    }

    @Override
    @NotNull
    public String getDatabaseDDLAuto() {
        return getStringValue(DATABASE_DDL_AUTO);
    }

    @Override
    @NotNull
    public String getDatabaseShowSQL() {
        return getStringValue(DATABASE_SHOW_SQL);
    }

    @NotNull
    @Override
    public String getSecondLvlCache() {
        return getStringValue(DATABASE_2LC);
    }

    @NotNull
    @Override
    public String getFactoryClass() {
        return getStringValue(DATABASE_FACTORY_CLASS);
    }

    @NotNull
    @Override
    public String getUseQueryCache() {
        return getStringValue(DATABASE_USE_QUERY_CACHE);
    }

    @NotNull
    @Override
    public String getUseMinimalPuts() {
        return getStringValue(DATABASE_USE_MIN_PUTS);
    }

    @NotNull
    @Override
    public String getRegionPrefix() {
        return getStringValue(DATABASE_REGION_PREFIX);
    }

    @NotNull
    @Override
    public String getConfigFilePath() {
        return getStringValue(DATABASE_CONFIG_FILE_PATH);
    }

    @NotNull
    @Override
    public String getExternalLoggerUrl() {
        return getStringValue(EXTERNAL_LOGGER_URL);
    }

    @NotNull
    @Override
    public String getExternalLoggerQueue() {
        return getStringValue(EXTERNAL_LOGGER_QUEUE);
    }

    @NotNull
    @Override
    public String getEmptyValue() {
        return EMPTY_VALUE;
    }

    @NotNull
    private String getStringValue(@NotNull final String key) {
        return getStringValue(key, EMPTY_VALUE);
    }

    @NotNull
    private String getStringValue(@NotNull final String key, @NotNull final String defaultValue) {
        // в параметрах запуска
        if (System.getProperties().containsKey(key)) return System.getProperties().getProperty(key);
        // в переменных среды
        @NotNull String envKey = getEnvKey(key);
        if (System.getenv().containsKey(envKey)) return System.getenv(envKey);
        // в файле настройки
        return properties.getProperty(key, defaultValue);
    }

    @NotNull
    private String getEnvKey(@NotNull final String key) {
        return key.replace(".", "_").toUpperCase();
    }

    @NotNull
    private Integer getIntValue(@NotNull final String key, @NotNull final String defaultValue) {
        return Integer.parseInt(getStringValue(key, defaultValue));
    }

}

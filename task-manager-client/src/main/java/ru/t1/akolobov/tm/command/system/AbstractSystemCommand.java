package ru.t1.akolobov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.akolobov.tm.api.endpoint.ISystemEndpoint;
import ru.t1.akolobov.tm.api.service.ICommandService;
import ru.t1.akolobov.tm.api.service.IPropertyService;
import ru.t1.akolobov.tm.command.AbstractCommand;

@Component
public abstract class AbstractSystemCommand extends AbstractCommand {

    @NotNull
    @Autowired
    protected ICommandService commandService;

    @NotNull
    @Autowired
    protected IPropertyService propertyService;

    @NotNull
    @Autowired
    protected ISystemEndpoint systemEndpoint;

    @NotNull
    protected ICommandService getCommandService() {
        return this.commandService;
    }

    @NotNull
    protected IPropertyService getPropertyService() {
        return this.propertyService;
    }

    protected ISystemEndpoint getSystemEndpoint() {
        return this.systemEndpoint;
    }

}

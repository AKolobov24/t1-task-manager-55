package ru.t1.akolobov.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.akolobov.tm.api.repository.ICommandRepository;
import ru.t1.akolobov.tm.api.service.ICommandService;
import ru.t1.akolobov.tm.api.service.ILoggerService;
import ru.t1.akolobov.tm.api.service.IPropertyService;
import ru.t1.akolobov.tm.api.service.ITokenService;
import ru.t1.akolobov.tm.command.AbstractCommand;
import ru.t1.akolobov.tm.command.system.ApplicationExitCommand;
import ru.t1.akolobov.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.akolobov.tm.exception.system.CommandNotSupportedException;
import ru.t1.akolobov.tm.util.SystemUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

@Component
public final class Bootstrap {

    @NotNull
    @Autowired
    private AbstractCommand[] abstractCommands;

    @NotNull
    @Autowired
    private ICommandRepository commandRepository;

    @NotNull
    @Autowired
    private ICommandService commandService;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @NotNull
    @Autowired
    private ITokenService tokenService;

    @NotNull
    @Autowired
    private FileScanner fileScanner;

    @SneakyThrows
    private void initPid() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void prepareStartup() {
        initPid();
        loggerService.info("** WELCOME TO TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        fileScanner.start();
    }

    private void prepareShutdown() {
        loggerService.info("** TASK MANAGER IS SHUTTING DOWN **");
        fileScanner.stop();
    }

    private void processArguments(@Nullable final String[] args) {
        if (args == null || args.length == 0) {
            return;
        }
        @Nullable final String arg = args[0];
        processArgument(arg);
        exit();
    }

    private void processCommands() {
        @NotNull final Scanner scanner = new Scanner(System.in);
        @Nullable String command;
        @Nullable AbstractCommand abstractCommand = null;
        while (!(abstractCommand instanceof ApplicationExitCommand)) {
            try {
                System.out.println("ENTER COMMAND:");
                command = scanner.nextLine();
                abstractCommand = processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    private void processArgument(@Nullable final String arg) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(arg);
        if (abstractCommand == null) throw new ArgumentNotSupportedException();
        abstractCommand.execute();
    }

    @SneakyThrows
    private void registry() {
        for (@NotNull final AbstractCommand command : abstractCommands) {
            registry(command);
        }
    }

    private void registry(@NotNull final AbstractCommand command) {
        commandService.add(command);
    }

    private void exit() {
        System.exit(0);
    }

    public void run(@Nullable final String[] args) {
        registry();
        processArguments(args);
        prepareStartup();
        processCommands();
    }

    public AbstractCommand processCommand(@Nullable final String command) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException();
        abstractCommand.execute();
        return abstractCommand;
    }

}

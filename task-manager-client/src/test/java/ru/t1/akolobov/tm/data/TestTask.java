package ru.t1.akolobov.tm.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.dto.model.TaskDto;

import java.util.ArrayList;
import java.util.List;

public final class TestTask {

    @NotNull
    public static TaskDto createTask() {
        return new TaskDto("new-task", "new-task-desc");
    }

    @NotNull
    public static List<TaskDto> createTaskList(int size) {
        @NotNull List<TaskDto> taskList = new ArrayList<>();
        for (int i = 1; i <= size; i++) {
            @NotNull TaskDto task = new TaskDto("task-" + i, "task-" + i + "desc");
            taskList.add(task);
        }
        return taskList;
    }


}

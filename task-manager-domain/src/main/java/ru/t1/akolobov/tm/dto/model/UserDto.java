package ru.t1.akolobov.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.enumerated.Role;

import javax.persistence.*;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_user")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class UserDto extends AbstractDtoModel {

    private static final long serialVersionUID = 1;

    @Column
    @NotNull
    private String login;

    @NotNull
    @Column(name = "password_hash")
    private String passwordHash;

    @Nullable
    @Column(name = "first_name")
    private String firstName;

    @Nullable
    @Column(name = "last_name")
    private String lastName;

    @Nullable
    @Column(name = "middle_name")
    private String middleName;

    @Column
    @Nullable
    private String email;

    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    private Role role = Role.USUAL;

    @Column
    private boolean locked = false;

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj instanceof UserDto) {
            UserDto anotherUser = (UserDto) obj;
            return this.getId().equals(anotherUser.getId()) &&
                    this.getLogin().equals(anotherUser.getLogin());
        }
        return false;
    }

}
